package com.owen.videoresize.mediacodec

import android.media.MediaCodec
import android.media.MediaExtractor
import android.media.MediaFormat
import android.util.Log
import java.nio.ByteBuffer
import java.nio.ByteOrder

class AudioCompressor(
    private val mediaExtractor: MediaExtractor,
    private val trackInfo: TrackInfo,
    private val muxRender: MuxRender
) {

    //END_OF_STREAM flag
    private var isEOS = false

    private var bufferSize = if (trackInfo.audioMediaFormat.containsKey(MediaFormat.KEY_MAX_INPUT_SIZE)) trackInfo.audioMediaFormat.getInteger(MediaFormat.KEY_MAX_INPUT_SIZE) else (64 * 1024)

    private lateinit var buffer: ByteBuffer

    private val bufferInfo = MediaCodec.BufferInfo()

    var writtenPresentationTimeUs: Long = 0
        private set

    init {
        muxRender.setOutputFormat(MuxRender.SampleType.AUDIO, trackInfo.audioMediaFormat)
        initBuffer(bufferSize)
    }

    private fun initBuffer(size: Int) {
        buffer = ByteBuffer.allocateDirect(size).order(ByteOrder.nativeOrder())
    }

    fun stepPipeline(): Boolean {

        if (isEOS) return false

        val trackIndex = mediaExtractor.sampleTrackIndex

        if (trackIndex < 0) {
            buffer.clear()
            bufferInfo.set(0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM)
            isEOS = true
            return isEOS
        }

        if (trackIndex != trackInfo.audioIndex) return false

        buffer.clear()

        val sampleSize = mediaExtractor.readSampleData(buffer, 0)

        if (sampleSize > bufferSize) {
            Log.d(TAG, "sample size need to smaller than buffer size, resizing buffer size ")
            bufferSize = sampleSize * 2
            initBuffer(bufferSize)
        }

        val isKeyFrame = mediaExtractor.sampleFlags and MediaExtractor.SAMPLE_FLAG_SYNC != 0
        val flags = if (isKeyFrame) MediaCodec.BUFFER_FLAG_KEY_FRAME else 0

        if (mediaExtractor.sampleTime >= 0) {
            bufferInfo.set(0, sampleSize, mediaExtractor.sampleTime, flags)
            muxRender.writeSampleData(MuxRender.SampleType.AUDIO, buffer, bufferInfo)
        }

        writtenPresentationTimeUs = mediaExtractor.sampleTime
        mediaExtractor.advance()

        return true
    }

    fun isFinish() = isEOS

    companion object {
        private const val TAG = "AudioCompressor"
    }
}