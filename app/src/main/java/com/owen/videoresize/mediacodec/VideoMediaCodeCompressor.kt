package com.owen.videoresize.mediacodec

import android.content.Context
import android.media.*
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.view.Surface
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.atomic.AtomicReference
import android.media.MediaMetadataRetriever
import android.util.Size
import kotlin.math.min


class VideoMediaCodeCompressor(private val callback: CallBack) {

    private val job = Job()

    private val scrop = CoroutineScope(Dispatchers.IO + job)

    private lateinit var mediaExtractor: MediaExtractor

    private lateinit var trackInfo: TrackInfo

    private lateinit var muxer: MediaMuxer

    private lateinit var muxRender: MuxRender

    var resizeFactor = .8f

    private val bufferInfo = MediaCodec.BufferInfo()

    private lateinit var encoder: MediaCodec

    private lateinit var decoder: MediaCodec

    private lateinit var inputSurface: InputSurface

    private lateinit var outputSurface: OutputSurface

    private var videoTrackIndex = -1

    private var audioTrackIndex = -1

    private var isExtractorEOS: Boolean = false
    private var isDecoderEOS: Boolean = false
    private var isEncoderEOS: Boolean = false

    private var writtenPresentationTimeUs: Long = 0

    private var actualOutputFormat: MediaFormat? = null

    private var durationUs = 0L

    private var mediaMetadataRetriever: MediaMetadataRetriever? = null

    private var filePath: String? = null

    private lateinit var audioCompressor: AudioCompressor

    fun compress(context: Context, uri: Uri) {
        Log.d(TAG, "compress")
        launchDataLoad {
            doCompress(context, uri)
        }
    }

    private fun getOutputPath(): String {
        return if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()) {
            "${Environment.getExternalStorageDirectory().absolutePath}/${Environment.DIRECTORY_MOVIES}/${SimpleDateFormat("yyyyMM_dd-HHmmss").format(Date())}_test.mp4"
        } else{
            "${Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES)}/${SimpleDateFormat("yyyyMM_dd-HHmmss").format(Date())}_test_1.mp4"
        }
    }

    private suspend fun doCompress(context: Context, uri: Uri) {
        Log.d(TAG, "doCompress")
        withContext(Dispatchers.IO) {

            // init track info
            trackInfo = initTrackInfo(context, uri) ?: throw RuntimeException("no trackInfo")

            videoTrackIndex = trackInfo.videoIndex

            audioTrackIndex = trackInfo.audioIndex

            setupEncoder()

            setupDecoder()

            muxRender = MuxRender(MediaMuxer(getOutputPath().also { filePath = it }, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4).also { muxer = it })

            mediaExtractor.selectTrack(videoTrackIndex)

            setupAudio()

            mediaExtractor.selectTrack(audioTrackIndex)

            runPipelines()

//            runPipelinesNoAudio()

            releaseOutputResources()
        }
    }

    private fun initTrackInfo(context: Context, uri: Uri): TrackInfo? {
        Log.d(TAG, "doCompress")

        val descriptor = context.contentResolver.openFileDescriptor(uri, "rw") ?: return null

        // video info
        mediaExtractor = MediaExtractor().apply {
            setDataSource(descriptor.fileDescriptor)
        }

        mediaMetadataRetriever = MediaMetadataRetriever().apply {
            setDataSource(descriptor.fileDescriptor)
            durationUs = try {
                (extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)).toLong() * 1000
            } catch (e: NumberFormatException) {
                -1
            }
            Log.d(TAG, "Duration (us): $durationUs")
        }

        return TrackInfo.from(mediaExtractor)
    }

    private fun calcBitRate(width: Int, height: Int): Int {
        val bitrate = (0.25 * 30.0 * width.toDouble() * height.toDouble()).toInt()
        Log.d(TAG, "bitrate=$bitrate")
        return bitrate
    }

    private fun getSize(): Size? {
        return mediaMetadataRetriever?.run {
            var width = (extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH).toInt() * resizeFactor).toInt()
            var height = (extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT).toInt() * resizeFactor).toInt()

            // Must multiple of 2
            if (width % 2 != 0) {
                width += 1
            }
            // Must multiple of 2
            if (height % 2 != 0) {
                height += 1
            }

            val rotate = mediaMetadataRetriever?.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION)?.toInt() ?: 0
            if (rotate == 0 || rotate == 180)
                Size(width, height)
            else
                Size(height, width)
        }
    }

    private fun initOutputVideoMediaFormat(): MediaFormat {
        val size = getSize()
        val width = size?.width ?: 0
        val height = size?.height ?: 0

        Log.d(TAG, "initOutputVideoMediaFormat width:$width, height: $height")

        return MediaFormat.createVideoFormat(MediaFormat.MIMETYPE_VIDEO_AVC, width, height).apply {
            setInteger(MediaFormat.KEY_BIT_RATE, OUTPUT_VIDEO_BIT_RATE)
            // Required but ignored by the encoder
            setInteger(MediaFormat.KEY_FRAME_RATE, OUTPUT_VIDEO_FRAME_RATE)
            setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, OUTPUT_VIDEO_IFRAME_INTERVAL)
            setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface)
        }
    }

    private fun setupEncoder() {
        val inputSurfaceReference = AtomicReference<Surface>()
        encoder = createVideoEncoder(initOutputVideoMediaFormat(), inputSurfaceReference)
        inputSurface = InputSurface(inputSurfaceReference.get()).apply { makeCurrent() }
    }

    private fun setupDecoder() {
        outputSurface =  OutputSurface()
        decoder = createVideoDecoder(trackInfo.videoMediaFormat, outputSurface.surface )
    }

    private fun createVideoDecoder(inputFormat: MediaFormat, surface: Surface): MediaCodec {
        return MediaCodec.createDecoderByType(getMimeTypeFor(inputFormat)).apply {
            configure(inputFormat, surface, null, 0)
            start()
        }
    }

    private fun createVideoEncoder(format: MediaFormat, surfaceReference: AtomicReference<Surface>): MediaCodec {
        val encoder = MediaCodec.createEncoderByType(getMimeTypeFor(format))
        encoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE)
        // Must be called before start() is.
        surfaceReference.set(encoder.createInputSurface())

        encoder.start()
        return encoder
    }


    private fun setupAudio() {
        audioCompressor = AudioCompressor(mediaExtractor, trackInfo, muxRender)
    }

    private fun runPipelines() {
        var loopCount: Long = 0

        while (!(isFinish() && audioCompressor.isFinish())) {
            val stepped = stepPipeline() || audioCompressor.stepPipeline()
            loopCount++

            if (durationUs > 0 && loopCount % PROGRESS_INTERVAL_STEPS == 0L) {
                val videoProgress = if (isFinish()) 1.0 else min(1.0, writtenPresentationTimeUs.toDouble() / durationUs)
                val audioProgress = if (isFinish()) 1.0 else min(1.0, audioCompressor.writtenPresentationTimeUs.toDouble() / durationUs)

                val progress = (videoProgress + audioProgress) / 2

                callback.onProgress(progress)
            }

            if (!stepped) {
                try {
                    Thread.sleep(10)
                } catch (e: InterruptedException) {
                    // nothing to do
                }
            }

        }

        callback.OnSuccess(filePath!!)
    }

    private fun runPipelinesNoAudio() {
        var loopCount: Long = 0

        while (!isFinish()) {
            val stepped = stepPipeline()
            loopCount++
            if (durationUs > 0 && loopCount % PROGRESS_INTERVAL_STEPS == 0L) {
                val videoProgress = if (isFinish()) 1.0 else min(1.0, writtenPresentationTimeUs.toDouble() / durationUs)

                callback.onProgress(videoProgress)
            }
            if (!stepped) {
                try {
                    Thread.sleep(10)
                } catch (e: InterruptedException) {
                    // nothing to do
                }
            }
        }

        callback.OnSuccess(filePath!!)
    }

    private fun stepPipeline(): Boolean {
        var busy = false

        var status: Int
        while (drainEncoder() != DRAIN_STATE_NONE) {
            busy = true
        }

        do {
            status = drainDecoder()
            if (status != DRAIN_STATE_NONE) {
                busy = true
            }
            // NOTE: not repeating to keep from deadlock when encoder is full.
        } while (status == DRAIN_STATE_SHOULD_RETRY_IMMEDIATELY)

        while (drainExtractor() != DRAIN_STATE_NONE) {
            busy = true
        }

        return busy
    }

    private fun drainExtractor(): Int {
        if (isExtractorEOS) return DRAIN_STATE_NONE
        val trackIndex = mediaExtractor.sampleTrackIndex
        if (trackIndex >= 0 && trackIndex != this.videoTrackIndex) {
            return DRAIN_STATE_NONE
        }
        val result = decoder.dequeueInputBuffer(0)
        if (result < 0) return DRAIN_STATE_NONE
        if (trackIndex < 0) {
            isExtractorEOS = true
            decoder.queueInputBuffer(result, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM)
            return DRAIN_STATE_NONE
        }

        val sampleSizeCompat = mediaExtractor.readSampleData(decoder.getInputBuffer(result)!!, 0)
        val isKeyFrame = mediaExtractor.sampleFlags and MediaExtractor.SAMPLE_FLAG_SYNC != 0
        decoder.queueInputBuffer(
            result,
            0,
            sampleSizeCompat,
            mediaExtractor.sampleTime / 1,
            if (isKeyFrame) MediaCodec.BUFFER_FLAG_KEY_FRAME else 0
        )
        mediaExtractor.advance()
        return DRAIN_STATE_CONSUMED
    }

    private fun drainDecoder(): Int {
        if (isDecoderEOS) return DRAIN_STATE_NONE
        val result = decoder.dequeueOutputBuffer(bufferInfo, 0)
        when (result) {
            MediaCodec.INFO_TRY_AGAIN_LATER -> return DRAIN_STATE_NONE
            MediaCodec.INFO_OUTPUT_FORMAT_CHANGED -> return DRAIN_STATE_SHOULD_RETRY_IMMEDIATELY
            MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED -> return DRAIN_STATE_SHOULD_RETRY_IMMEDIATELY
        }
        if (bufferInfo.flags and MediaCodec.BUFFER_FLAG_END_OF_STREAM != 0) {
            encoder.signalEndOfInputStream()
            isDecoderEOS = true
            bufferInfo.size = 0
        }
        val doRender = (bufferInfo.size > 0)
        // NOTE: doRender will block if buffer (of encoder) is full.
        // Refer: http://bigflake.com/mediacodec/CameraToMpegTest.java.txt
        decoder.releaseOutputBuffer(result, doRender)
        if (doRender) {
            outputSurface.awaitNewImage()
            outputSurface.drawImage()
            inputSurface.setPresentationTime(bufferInfo.presentationTimeUs * 1000)
            inputSurface.swapBuffers()
        } else if (bufferInfo.presentationTimeUs != 0L) {
            writtenPresentationTimeUs = bufferInfo.presentationTimeUs
        }
        return DRAIN_STATE_CONSUMED
    }

    private fun drainEncoder(): Int {
        if (isEncoderEOS) return DRAIN_STATE_NONE
        val result = encoder.dequeueOutputBuffer(bufferInfo, 0)
        when (result) {
            MediaCodec.INFO_TRY_AGAIN_LATER -> return DRAIN_STATE_NONE
            MediaCodec.INFO_OUTPUT_FORMAT_CHANGED -> {
                actualOutputFormat?.run{ throw RuntimeException("Video output format changed twice.")}

                actualOutputFormat = encoder.outputFormat
                muxRender.setOutputFormat(MuxRender.SampleType.VIDEO, actualOutputFormat)
                muxRender.onSetOutputFormat()
                return DRAIN_STATE_SHOULD_RETRY_IMMEDIATELY
            }
            MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED -> return DRAIN_STATE_SHOULD_RETRY_IMMEDIATELY
        }

        actualOutputFormat ?: throw RuntimeException("Could not determine actual output format.")

        if ((bufferInfo.flags and MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
            isEncoderEOS = true
            bufferInfo.set(0, 0, 0, bufferInfo.flags)
        }
        if ((bufferInfo.flags and MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
            // SPS or PPS, which should be passed by MediaFormat.
            encoder.releaseOutputBuffer(result, false)
            return DRAIN_STATE_SHOULD_RETRY_IMMEDIATELY
        }
        muxRender.writeSampleData(MuxRender.SampleType.VIDEO, encoder.getOutputBuffer(result), bufferInfo)
        writtenPresentationTimeUs = bufferInfo.presentationTimeUs
        encoder.releaseOutputBuffer(result, false)
        return DRAIN_STATE_CONSUMED
    }

    fun destory() {
        job.cancel()

        releaseOutputResources()
    }

    fun isFinish() = isEncoderEOS

    private fun releaseOutputResources() {
        inputSurface.release()
        outputSurface.release()

        encoder.stop()
        encoder.release()

        decoder.stop()
        decoder.release()

        muxer.stop()
        muxer.release()

        mediaExtractor.release()

        mediaMetadataRetriever?.release()
    }

    private fun getMimeTypeFor(format: MediaFormat): String = format.getString(MediaFormat.KEY_MIME)

    private fun launchDataLoad(block: suspend () -> Unit): Job {
        return scrop.launch {
            try {
                block()
            } catch (error: Throwable) {
                error.printStackTrace()
            } finally {
            }
        }
    }

    interface CallBack {

        fun OnSuccess(path: String)

        fun OnFailure()

        fun onProgress(videoProgress: Double)
    }

    companion object {

        const val TAG = "VideoMediaCode"

        private const val DRAIN_STATE_NONE = 0
        private const val DRAIN_STATE_SHOULD_RETRY_IMMEDIATELY = 1
        private const val DRAIN_STATE_CONSUMED = 2

        private const val PROGRESS_INTERVAL_STEPS: Long = 10

        private const val OUTPUT_VIDEO_BIT_RATE = 2000000 // 2Mbps
        private const val OUTPUT_VIDEO_FRAME_RATE = 15 // 15fps
        private const val OUTPUT_VIDEO_IFRAME_INTERVAL = 10 // 10 seconds between I-frames
    }
}