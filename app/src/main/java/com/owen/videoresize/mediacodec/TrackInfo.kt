package com.owen.videoresize.mediacodec

import android.media.MediaExtractor
import android.media.MediaFormat

class TrackInfo {

    var videoIndex = -1
        private set

    var audioIndex = -1
        private set

    lateinit var videoMediaFormat: MediaFormat
        private set

    lateinit var audioMediaFormat: MediaFormat
        private set

    companion object {

        fun from(mediaExtractor: MediaExtractor) = TrackInfo().apply {

            for (i in 0 until mediaExtractor.trackCount) {
                val trackFormat = mediaExtractor.getTrackFormat(i)
                val mine = trackFormat.getString(MediaFormat.KEY_MIME)
                when {
                    mine?.startsWith("video/") == true -> {
                        videoIndex = i
                        videoMediaFormat = trackFormat
                    }
                    mine?.startsWith("audio/") == true -> {
                        audioIndex = i
                        audioMediaFormat = trackFormat
                    }
                }
            }
        }
    }
}