package com.owen.videoresize

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.media.MediaMetadataRetriever
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.owen.videoresize.mediacodec.VideoMediaCodeCompressor
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {

    private lateinit var uri: Uri

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_CANCELED) return

        // Result code is RESULT_OK only if the user selects an Image
        when (requestCode) {
            GALLERY_REQUEST_CODE -> {
                Log.d(TAG, "GALLERY_REQUEST_CODE data:${data?.data}")
                uri = data?.data ?: return

                setupVideo(uri)
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_select.setOnClickListener { pickFromGallery() }

        btn_resize.setOnClickListener { resizeVideo()  }
    }

    private fun pickFromGallery() {
        //Create an Intent with action as ACTION_PICK
        val intent = Intent(Intent.ACTION_PICK)
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.type = "video/*"
        // Launching the Intent
        startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }

    private fun setupVideo(uri: Uri) {
        CoroutineScope(Dispatchers.Main).launch {
            progressBar.progress = 0

            val video = getVideoInfo(uri)

            Log.d(TAG, "gallery path: ${video?.path}")
            textview_info.text = "displayName: ${video?.displayName}, duration: ${video?.duration}, resolution: ${video?.resolution}, size: ${video?.size}, mine: ${video?.mimeType}"

            videoview.setVideoURI(uri)
            videoview.start()
        }
    }

    private fun resizeVideo() {
        val startTimestamp: Long = System.currentTimeMillis()
        val compressor = VideoMediaCodeCompressor(object : VideoMediaCodeCompressor.CallBack{
            override fun onProgress(videoProgress: Double) {
                Log.d(TAG, "onProgress: $videoProgress")
                progressBar.progress = (videoProgress * 100).toInt()
            }

            override fun OnSuccess(path: String) {
                CoroutineScope(Dispatchers.Main).launch {
                    progressBar.progress = 100

                    val compressorDuration = System.currentTimeMillis() - startTimestamp

                    Log.d(TAG, "compressorDuration: $compressorDuration")

                    Toast.makeText(this@MainActivity, "compressor success. compressor duration: ${compressorDuration / 1000} sec", Toast.LENGTH_LONG).show()

                    exportMp4ToGallery(path)?.also { uri = it }

                    Thread.sleep(1000)

                    setupVideo(uri)
                }
            }

            override fun OnFailure() {

            }
        })
        Toast.makeText(this@MainActivity, "compressor start.", Toast.LENGTH_LONG).show()
        compressor.compress(this@MainActivity, uri = this@MainActivity.uri)
    }

    private suspend fun getVideoInfo(uri: Uri): VideoInfo? {
        return withContext(Dispatchers.IO) {

            contentResolver.query(
                uri,
                VIDEO_COLUMNS,
                null,
                null,
                MediaStore.Video.Media.DATE_MODIFIED
            ).use { cursor ->
                if (cursor == null || cursor.count == 0) return@withContext null

                cursor.moveToFirst()
                val path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA))
                val displayName = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DISPLAY_NAME))
                val size = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.Media.SIZE))

                val descriptor = contentResolver.openFileDescriptor(uri, "r")!!
                // support 3pg、mp4
                val metadata = MediaMetadataRetriever().apply { setDataSource(descriptor.fileDescriptor!!) }
                VideoInfo(
                    path = path,
                    displayName = displayName,
                    title = metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE),
                    duration = metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION).toLong(),
                    artist = metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST),
                    album = metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM),
                    resolution = metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION),
                    mimeType = metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_MIMETYPE),
                    size = size
                )
            }
        }
    }

    private suspend fun exportMp4ToGallery(filePath: String): Uri? {
        return withContext(Dispatchers.IO) {
            val values = ContentValues(2).apply {
                put(MediaStore.Video.Media.MIME_TYPE, OUTPUT_MINE_TYPE)
                put(MediaStore.Video.Media.DATA, filePath)
            }

            val uri = contentResolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values)
            MediaScannerConnection.scanFile(this@MainActivity, arrayOf(filePath), arrayOf(OUTPUT_MINE_TYPE), null)

            uri
        }
    }

    companion object {
        const val TAG = "MainActivity"
        const val GALLERY_REQUEST_CODE = 1001

        const val OUTPUT_MINE_TYPE = "video/mp4"

        private val VIDEO_COLUMNS = arrayOf(
            MediaStore.Video.Media.DATA,
            MediaStore.Video.Media.DISPLAY_NAME,
            MediaStore.Video.Media.SIZE
        )
    }
}
