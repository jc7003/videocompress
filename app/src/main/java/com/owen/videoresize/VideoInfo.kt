package com.owen.videoresize

class VideoInfo(
    var path: String,
    var displayName: String? = null,
    var title: String? = null,
    var duration: Long = 0,
    var artist: String? = null,
    var resolution: String? = null,
    var album: String? = null,
    var mimeType: String? = null,
    var size: Long = 0
)